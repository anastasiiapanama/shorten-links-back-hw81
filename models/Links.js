const mongoose = require('mongoose');
const {nanoid} = require("nanoid/non-secure");

const LinksSchema = new mongoose.Schema({
    _id: {
      type: String,
      default: () => nanoid(10)
    },
    originalUrl: {
        type: String,
        required: true
    },
    shortUrl: {
        type: String,
        default: () => nanoid(7)
    }
});

const Links = mongoose.model('Links', LinksSchema);
module.exports = Links;
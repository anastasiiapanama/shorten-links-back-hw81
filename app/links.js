const express = require('express');

const Links = require("../models/Links");

const router = express.Router();

router.get('/:shortUrl', async (req, res) => {
    try {
        const link = await Links.findOne({shortUrl: req.params.shortUrl});

        if (link) {
            res.status(301).redirect(link.originalUrl)
        } else (
            res.sendStatus(404)
        )

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    try {
        const linkRequestData = req.body;
        const link = new Links(linkRequestData);
        await link.save();

        res.send(link.shortUrl);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;
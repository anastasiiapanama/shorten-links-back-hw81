const express = require('express');
const cors = require("cors");
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');

const links = require('./app/links');

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/links', links);

const run = async () => {

    await mongoose.connect('mongodb://localhost/links', {useNewUrlParser: true, useUnifiedTopology: true});

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
};

run().catch(console.error);